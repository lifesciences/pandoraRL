import os
import torch
import pickle
import datetime
import contextlib
import numpy as np
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt

from rlvs.molecule_world.env import GraphEnv
from rlvs.agents.dqn_agent import DQNAgentGNN
from rlvs.config import Config
from rlvs.constants import FeatureNames
from rlvs.molecule_world.scoring import Reward
from rlvs.molecule_world.datastore.datasets import DataStore

from gnn_explainer import Explainer
from rlvs.molecule_world.scoring.reward import Reward

class GNNExplainer:
    def __init__(self, epochs=200, return_type='raw', feat_mask_type='feature', log=False):
        self.config = None
        self.env = None
        self.agent = None
        self.model = None
        self.states_df = None
        self.feature_labels = None
        self.train_data = None
        self.test_data = None

        self.USE_CUDA = torch.cuda.is_available()
        self.device = torch.device('cuda' if self.USE_CUDA else 'cpu')
        self.explainer = Explainer(self.model, epochs=epochs, return_type=return_type, feat_mask_type=feat_mask_type, log=log)
        
    def init_env(self, config_path, test=True):
        Config.init(config_path)
        self.config = Config.get_instance()
        self.feature_labels = FeatureNames.feature_labels(self.config.node_features)
        self.env = GraphEnv(single_step=np.array(self.config.single_step), test=test)
        self.train_data_dict = {cmplx.protein.path.split('/')[-2].lower():cmplx for cmplx in DataStore.DATA}
        self.test_data_dict = {cmplx.protein.path.split('/')[-2].lower():cmplx for cmplx in DataStore.TEST_DATA}

    def set_model(self, model_path):
        self.agent = DQNAgentGNN(
            self.env,
            weights_path=None, #path_prefix+"weights_intermediate",
            complex_path=None #path_prefix+"ligand_intermediate"
        )

        self.agent.load_weights(model_path, None)
        self.model = self.agent._actor
        self.explainer.model = self.model

    def _get_data(self, m_complex):
        grph = m_complex.complex_graph
        data = m_complex.data
        data = data.to(self.device)
        return grph, data

    def _run_gnn_explainer(self, data, grph):
        node_feat_mask, edge_feat_mask, edge_mask = self.explainer.explain_graph(data)
        node_feat_mask, edge_feat_mask, edge_mask = node_feat_mask.cpu(), edge_feat_mask.cpu(), edge_mask.cpu()
        _, G = self.explainer.visualize_subgraph(node_idx=-1, edge_index=data.edge_index, edge_mask=edge_mask)#, threshold=0.7)
        mol_type_col_mapping = {0: '#abdda4', 1:'#3288bd'}
        node_props = {
                idx:{
                    'atom_idx': d['atom'].atom_idx,
                    'name': d['atom'].name,
                    'residue': d['atom'].residue.name if d['atom'].residue else 'NA',
                    'mol_type': 'protein' if d['molecule_type'] == 0 else 'ligand',
                    'mol_type_col': mol_type_col_mapping[d['molecule_type']]
                    }
                for idx, d in list(grph.nodes(data=True))
            }
        nx.set_node_attributes(G, values = node_props)

        return node_feat_mask, edge_feat_mask, edge_mask, G

    def _get_imp_edges(self, G, grph_edges, edge_mask, threshold):
        edge_idx_filtered = torch.where(edge_mask > threshold)[0]
        edge_list = list(G.edges)
        return [
            (
                G.nodes.get(edge_list[idx][0]),
                G.nodes.get(edge_list[idx][1]),
                {'bond_type': grph_edges.get(edge_list[idx])['bond'].bond_type,
                'encoding': grph_edges.get(edge_list[idx])['bond'].encoding()}
            ) for idx in edge_idx_filtered
        ]
    
    def _get_step_state(self, episode_length, m_complex_t, feature_labels,
                        node_feat_mask, edge_feat_mask, edge_mask,
                        imp_edges, molecule_action, reward, terminal):
        step_state = {}
        step_state['step'] = episode_length
        step_state['complex'] = m_complex_t.ligand.path.split('/')[-2]
        step_state['ligand_centroid'] = m_complex_t.ligand.get_centroid()
        step_state['ligand_centroid_sepeartion'] = m_complex_t.ligand_centroid_saperation
        step_state['rmsd'] = m_complex_t.rmsd
        step_state['vina_score'] = m_complex_t.vina_score()
        step_state['n_feat_mask'] = dict(zip(feature_labels, node_feat_mask))
        step_state['e_feat_mask'] = edge_feat_mask
        step_state['edge_mask'] = edge_mask
        step_state['imp_edge'] = imp_edges
        step_state['action'] = molecule_action
        step_state['reward'] = reward
        step_state['terminal'] = terminal

        return step_state
    
    def get_complex(self, test=True, cmplx=None, start_delta=None):
        if cmplx is not None:
            m_complex_t = self.test_data_dict[cmplx.lower()] if test else self.train_data_dict[cmplx.lower()]
        else:
            m_complex_t = np.random.choice(list(self.test_data.values()) if test else list(self.train_data.values()))
        
        if start_delta is not None:
            m_complex_t.reset_ligand()
            m_complex_t.update_edges()
            m_complex_t.update_pose(*start_delta)
            m_complex_t.update_edges()
        else:
            m_complex_t.randomize_ligand(self.env.action_space.n_outputs)
        
        return m_complex_t

    def save(self, output_path):
        with open(output_path, "wb") as f:
            pickle.dump(self.states_df, f)

    def run(self, max_episode_length, threshold=0.5, test=True, cmplx=None, start_delta=None):
        m_complex_t = self.get_complex(test=test, cmplx=cmplx, start_delta=start_delta)
        self.env._complex = m_complex_t
        self.env.reward = Reward.get_reward_function(m_complex_t)
        self.env.reward()
        episode_return, episode_length, terminal = 0, 0, False
        states = []
        while not (episode_length == max_episode_length):
            grph, data = self._get_data(m_complex_t)
            node_feat_mask, edge_feat_mask, \
                edge_mask, G = self._run_gnn_explainer(data, grph)
            imp_edges = self._get_imp_edges(G, grph.edges, edge_mask, threshold)
            data = data.cuda() if self.USE_CUDA else data
            with open(os.devnull, "w") as f, contextlib.redirect_stdout(f):
                action = self.agent.act(data, test=True)
            data.cpu()
            molecule_action = self.env.action_space.get_action(action)
            reward, terminal = self.env.step(molecule_action)
            
            step_state = self._get_step_state(episode_length, m_complex_t, self.feature_labels,
                                                    node_feat_mask, edge_feat_mask, edge_mask,
                                                    imp_edges, molecule_action, reward, terminal)

            print(f"Step: {episode_length:03d} ---> Reward: {reward} ---> RMSD: {m_complex_t.rmsd} ---> Action: {action}")
            states.append(step_state)

            episode_return += reward
            episode_length += 1
        
        self.states_df = pd.DataFrame(states)