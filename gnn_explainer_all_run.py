import os
import numpy as np
import pandas as pd
from glob import glob

from extract_pose import extract_poses
from gnn_explainer_env import GNNExplainer
from rlvs.agents.metrices import Metric

# model_path = os.getenv('MPATH', None)
# config_path = os.getenv('CONFIG', './config.json')

get_episode_num = lambda path: int(path.split('_')[-2]) if path.split('_')[-2] != 'final' else 10000

gnn_explain = GNNExplainer()
metric_file_path = "../weights/model_fg_blossom4/run_tran_fg_blossom4__final_metric.npy"
log_file_path = "../weights/model_fg_blossom4/run_tran_fg_blossom4_training_log.log"
config_path = "../weights/model_fg_blossom4/config.json"
model_folder_path = "../weights/model_fg_blossom4/weights/"
output_folder = "../gnn_out"

n_steps = 100

model_paths = sorted(glob(model_folder_path+'*'), key=get_episode_num)


def get_ep_complex_df(metric_file_path):
    # metric files are large, hence local reference
    metric = Metric.load(metric_file_path, root=False)
    ep_complex_df = metric.generate_data_frame()
    ep_complex_df = ep_complex_df[['episode', 'molecule']]
    return ep_complex_df


ep_complex_df = get_ep_complex_df(metric_file_path)

train_ep_complex_dict = {}
for ep, grp in ep_complex_df.groupby('episode'):
    train_ep_complex_dict[ep] = tuple(grp['molecule'][0]) if isinstance(grp['molecule'][0], pd.Series) else tuple([grp['molecule'][0]])

train_ep_pose_dict, test_ep_pose_dict = extract_poses(log_file_path)  # reuse metric file

gnn_explain.init_env(config_path)

for model_path in model_paths:
    ep = get_episode_num(model_path)
    if ep != 10000:
        gnn_explain.set_model(model_path)
        complexes = train_ep_complex_dict[ep]
        poses = train_ep_pose_dict[ep]
        for cmplx, pose in zip(complexes, poses):
            print(f"{'#'*10} Runnig for E: {ep} with comples: {cmplx} starting from {pose} {'#'*10}")
            cmplx = cmplx.split('_')[0]
            gnn_explain.run(n_steps, test=False, cmplx=cmplx, start_delta=pose)
            gnn_explain.save(output_folder+f"/gnn_explainer_{ep}_{cmplx}.pkl")
