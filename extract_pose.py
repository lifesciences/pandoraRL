import re

def extract_poses(file):

    with open(file) as f:
        f = f.readlines()
        
    count = 0

    episode_info = []
    train_list = []
    test_list = []
    all_episode_pose_info_train = {}
    all_episode_pose_info_test = {}

    important = []
    index_number = []

    save_episode = 0

    for line in f:
        important.append(line)
        
    for line in f:
        if "Randomized Pose: " in line:
            pose_info = line[:].replace("\n", "")
            pose_info = eval(re.sub(r'\d\s+',',',pose_info.split(':')[1].strip()))
        
            # all_episode_pose_info.append(pose_info)
            
            index_number.append(count)
            
            if "E:" in important[count + 1] and "Test Action: " not in important[count + 1]:
                # print(pose_info)
                # print(important[count + 1].split(",", 6)[3])
                
                ep = important[count + 1].split(",", 6)[3]
                ep = int(ep.split(':')[1].strip())
                train_list = all_episode_pose_info_train.get(ep, [])
                train_list.append(pose_info)
                all_episode_pose_info_train[ep] = train_list
                # all_episode_pose_info_train.append([important[count + 1].split(",", 6)[3], pose_info])
                
            if "Episode: " in important[count - 1] and "Test Action: " in important[count + 1]:
                save_episode = int(important[count - 1].split(" ", 5)[1])
                # print(important[count - 1].split(" ", 5)[1])
                
                test_list = all_episode_pose_info_test.get(save_episode - 1, [])
                test_list.append(eval(re.sub(r'\d\s+',',',important[count].replace("\n", "").split(':')[1].strip())))
                # all_episode_pose_info_test.append([f" E: {save_episode - 1}", important[count].replace("\n", "")])
                
                # print(all_episode_pose_info_test)
                
                i = 1
                
                dummy_count = count
                
                value_of_episode = int((important[count + i].split(",", 6)[3]).split(":", 1)[1])
                
                while "E: " in important[count + i] or "Randomized Pose: " in important[count + i]:
                    if "Randomized Pose: " in important[count + i] and value_of_episode <= 8:
                        test_list.append(eval(re.sub(r'\d\s+',',',important[count+i].replace("\n", "").split(':')[1].strip())))
                        # all_episode_pose_info_test.append([f" E: {save_episode - 1}", important[count + i].replace("\n", "")])

                    elif "Randomized Pose: " not in important[count + i]:
                        value_of_episode = int((important[count + i].split(",", 6)[3]).split(":", 1)[1])

                        if value_of_episode > 8:
                            break

                    i += 1
                        
                all_episode_pose_info_test[save_episode - 1] = test_list
                
        count += 1
    return all_episode_pose_info_train, all_episode_pose_info_test