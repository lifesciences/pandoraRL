import torch
from .gnn_explainer import Explainer
from .gnn_explainer_handler import GNNExplainerHandler
from rlvs.network import ActorDQN
import networkx as nx
from rlvs.agents.metrices import Metric
from rlvs.constants import FeatureNames
from rlvs.molecule_world.datastore import DataCache
import pandas as pd
import pickle
import os
import traceback
import time
from psutil import Process, NoSuchProcess
import threading

from rlvs.config import Config
import multiprocessing as mp

def get_process_status(pid):
    if pid == -1:
        return False
    try:
        proc = Process(pid)
        return proc.status() in [
            'running',
            'sleeping'
        ]
    except NoSuchProcess:
        return False


def save(output_path, df):
    with open(output_path, "wb") as f:
        pickle.dump(df, f)

def threaded(fn):
    def wrapper(*args, **kwargs):
        threading.Thread(target=fn, args=args, kwargs=kwargs).start()
    return wrapper    


class GNNExplainer:
    def __init__(
            self,
            feature_labels,
            model,
            paths,
            episode_details,
            test=False
        ):
        weight_path, metric_path, model_path = paths
        self.i_episode, self.episode_length, self.train_episode = episode_details

        self.data_cache = DataCache(weight_path)
        self.feature_labels = feature_labels
        self.test = test

        self.metric = Metric.load(metric_path)

        if self.test:
            self.metric = self.metric.test_metrices[int(self.train_episode)]
        else:
            self.metric = self.metric.molecule_metrices[self.i_episode]
            
        self.model = model

        self.explainer = Explainer(
            self.model,
            epochs=10,
            return_type='raw',
            feat_mask_type='feature',
            log=False
        )

    def get_metric_index(self, data_index):
        readings_per_mol = [len(met.actions) for met in self.metric]
        for idx, val in enumerate(readings_per_mol):
            if data_index >= val:
                data_index -= val
            else:
                return idx, data_index

    def _run_gnn_explainer(self, data, grph):
        node_feat_mask, edge_feat_mask, edge_mask = self.explainer.explain_graph(data)
        node_feat_mask = node_feat_mask.cpu()
        edge_feat_mask = edge_feat_mask.cpu()
        edge_mask = edge_mask.cpu()
        
        _, G = self.explainer.visualize_subgraph(
            node_idx=-1,
            edge_index=data.edge_index,
            edge_mask=edge_mask
        )  # , threshold=0.7)
        mol_type_col_mapping = {0: '#abdda4', 1: '#3288bd'}
        node_props = {
                idx: d
                for idx, d in list(grph.nodes(data=True))
            }
        nx.set_node_attributes(G, values=node_props)

        return node_feat_mask, edge_feat_mask, edge_mask, G

    def _get_imp_edges(self, G, grph_edges, edge_mask, threshold):
        edge_idx_filtered = torch.where(edge_mask > threshold)[0]
        edge_list = list(G.edges)
        return [
            (
                G.nodes.get(edge_list[idx][0]),
                G.nodes.get(edge_list[idx][1]),
                {
                    'bond_type': grph_edges.get(edge_list[idx])[
                        'bond_type'
                    ],
                    'encoding': grph_edges.get(edge_list[idx])[
                        'encoding'
                    ]
                }
            ) for idx in edge_idx_filtered
        ]

    def _get_step_state(self, episode_length,
                        node_feat_mask, edge_feat_mask, edge_mask,
                        imp_edges, molecule_action, reward, terminal):
        step_state = {}
        step_state['step'] = episode_length
        step_state['n_feat_mask'] = dict(zip(
            self.feature_labels, node_feat_mask
        ))
        step_state['e_feat_mask'] = edge_feat_mask
        step_state['edge_mask'] = edge_mask
        step_state['imp_edge'] = imp_edges
        step_state['action'] = molecule_action
        step_state['reward'] = reward
        step_state['terminal'] = terminal

        return step_state

    def run(
            self,
            threshold=0.5
    ):
        data_index = 0
        episode_return = 0
        states = []
        while data_index < self.episode_length:
            grph, data, rmsd = self.data_cache.get_data(
                self.i_episode,
                data_index,
                self.test,
                train_episode=self.train_episode
            )
            m_row, m_col = self.get_metric_index(data_index)
            node_feat_mask, edge_feat_mask, \
                edge_mask, G = self._run_gnn_explainer(data, grph)
            imp_edges = self._get_imp_edges(G, grph.edges, edge_mask, threshold)
            step_state = self._get_step_state(
                data_index, node_feat_mask,
                edge_feat_mask, edge_mask,
                imp_edges, self.metric[m_row].actions[m_col],
                self.metric[m_row].rewards[m_col], data_index == self.episode_length
            )

            self.data_cache.clear_data(
                self.i_episode,
                data_index,
                self.test,
                train_episode=self.train_episode
            )
            data_index += 1
            states.append(step_state)
            episode_return += self.metric[m_row].rewards[m_col]
            print(f"Step: {data_index} ---> Reward: {self.metric[m_row].rewards[m_col]} ---> Action: {self.metric[m_row].actions[m_col]} ---> RMSD: {rmsd}\n")

        states_df = pd.DataFrame(states)
        return states_df


def run_gnn_explainer(
        feature_labels,
        model,
        paths, output_path,
        episode_details,
        test=False
):
    gnn_explainer = GNNExplainer(
        feature_labels,
        model,
        paths,
        episode_details,
        test=test
    )

    df = gnn_explainer.run()
    save(output_path, df)


class GNNExplainerServer:
    output_folder = "../gnn_out"
    log_path = "../gnn_out/log.txt"

    def __init__(self):
        config = Config.get_instance()
        self.queue = []
        self.gnn_exp_handler = GNNExplainerHandler()
        self.gnn_exp_handler.subscribe(config.profiler_port)
        self.current_process_id = None

    @threaded
    def recieve_message(self):
        while True:
            message = self.gnn_exp_handler.recv()
            if message is not None:
                self.queue.append(message)

            if message == 'END':
                break

            time.sleep(0.1)


    def run_explainer(self):
        if not os.path.exists(self.output_folder):
            os.makedirs(self.output_folder)
        
        self.recieve_message()
        while True:
            if self.current_process_id is not None:
                if get_process_status(self.current_process_id):
                    time.sleep(1)
                    continue
                else:
                    self.current_process_id = None
            if len(self.queue) > 0:
                message = self.queue.pop(0)

                if message == 'END':
                    break

                kwargs = self.gnn_exp_handler.get_kwargs(message)
                self._launch_process(**kwargs)
                time.sleep(1)

    def _launch_process(
            self,
            node_features,
            input_shape,
            edge_shape,
            action_shape,
            learning_rate,
            tau, use_cuda,
            weight_path,
            i_episode,
            episode_length,
            test, train_episode=''
    ):
        feature_labels = FeatureNames.feature_labels(node_features)
        try:
            metric_path = f'{weight_path}'
                
            if test:
                model_path = f'{weight_path}_{train_episode}_actor'
                output_file = f"{self.output_folder}/gnn_explainer_{train_episode}_{i_episode}_test.pkl"

            else:
                model_path = f'{weight_path}_{i_episode}_actor'
                output_file = f"{self.output_folder}/gnn_explainer_{i_episode}_train.pkl"


            model = ActorDQN(
                input_shape,
                edge_shape,
                action_shape,
                learning_rate,
                tau
            )

            if not use_cuda:
                model.load_state_dict(torch.load(model_path))
            else:
                model.load_state_dict(torch.load(
                    model_path,
                    map_location=torch.device('cpu')
                ))

            ctx = mp.get_context('spawn')
            explainer_sub_process = ctx.Process(
                target=run_gnn_explainer,
                args=(
                    feature_labels,
                    model,
                    [weight_path, metric_path, model_path],
                    output_file,
                    [i_episode, episode_length, train_episode],
                    test
                )
            )
            explainer_sub_process.start()
            self.current_process_id = explainer_sub_process.pid
            print(explainer_sub_process)

        except Exception as e:
            with open(self.log_path, "a") as f:
                f.write(traceback.format_exc())
