import zmq

class GNNExplainerHandler:
    TOPIC = 'GNN_EXP'
    def __init__(self, *args, **kwargs):
        self.preamble = kwargs
        self.socket = None
        self.publisher = None
        self.enable_gnn_explainer = False

    def register(self, publisher, enable_gnn_explainer):
        self.enable_gnn_explainer = enable_gnn_explainer
        self.publisher = publisher

    def subscribe(self, port):
        context = zmq.Context()
        self.socket = context.socket(zmq.SUB)
        self.socket.setsockopt(zmq.SUBSCRIBE, b'')
        self.socket.connect(f'tcp://127.0.0.1:{port}')
        print('Listening on port: ', port)

    def send(self, py_obj):
        if self.enable_gnn_explainer:
            self.publisher.publish(self.TOPIC, {
            'preamble': self.preamble,
            'message': py_obj
        })

    def end(self):
        if self.enable_gnn_explainer:
            self.publisher.publish(self.TOPIC, 'END')

    def recv(self):
        message = self.socket.recv_pyobj()
        print('Recieved: ', message)
        return message.get(self.TOPIC, None)

    def get_kwargs(self, explainer_message):
        return {
            **explainer_message.get('preamble', {}),
            **explainer_message.get('message', {})
        }
        
