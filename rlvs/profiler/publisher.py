import zmq

class Publisher:
    def __init__(self, port):
        context = zmq.Context()
        self.socket = context.socket(zmq.PUB)
        self.socket.bind(f'tcp://127.0.0.1:{port}')

    def publish(self, topic, py_obj):
        self.socket.send_pyobj({
            topic: py_obj
        })
        
