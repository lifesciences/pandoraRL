import numpy as np
from rlvs.constants import ComplexConstants


class ActionSpace:
    __instance = None

    @classmethod
    def init(cls, action_bounds):
        cls.__instance = ActionSpace(action_bounds)

    @classmethod
    def get_instance(cls):
        if cls.__instance is None:
            raise Exception("Action space not initialized")

        return cls.__instance

    def __init__(self, action_bounds):
        self.action_bounds = np.asarray(action_bounds)
        self.n_outputs = self.action_bounds.shape[1]
        self.degree_of_freedom = self.n_outputs * 2

        self._action_vectors = [
            [-ComplexConstants.TRANSLATION_DELTA, 0, 0, 0, 0, 0],
            [ComplexConstants.TRANSLATION_DELTA, 0, 0, 0, 0, 0],
            [0, -ComplexConstants.TRANSLATION_DELTA, 0, 0, 0, 0],
            [0, ComplexConstants.TRANSLATION_DELTA, 0, 0, 0, 0],
            [0, 0, -ComplexConstants.TRANSLATION_DELTA, 0, 0, 0],
            [0, 0, ComplexConstants.TRANSLATION_DELTA, 0, 0, 0],
            [0, 0, 0, -ComplexConstants.ROTATION_DELTA, 0, 0],
            [0, 0, 0, ComplexConstants.ROTATION_DELTA, 0, 0],
            [0, 0, 0, 0, -ComplexConstants.ROTATION_DELTA, 0],
            [0, 0, 0, 0, ComplexConstants.ROTATION_DELTA, 0],
            [0, 0, 0, 0, 0, -ComplexConstants.ROTATION_DELTA],
            [0, 0, 0, 0, 0, ComplexConstants.ROTATION_DELTA]
        ]

    def get_action(self, predicted_action_index):
        return self._action_vectors[predicted_action_index]

    def sample(self):
        return np.diff(
            self.action_bounds, axis=0
        ).flatten() * np.random.random_sample(
            self.action_bounds[0].shape
        ) + self.action_bounds[0]
