import numpy as np
from .rmsd import RMSD
from .vina_score import VinaScore

from rlvs.config import Config
from rlvs.constants import ComplexConstants


class Reward:
    __reward_functions = {
        'rmsd': lambda pl_complex: RMSDReward(pl_complex),
        'vina': lambda pl_complex: VinaReward(pl_complex),
        'jaccard': lambda pl_complex: JaccardIndex(pl_complex),
        'graph_similarity': lambda pl_complex: GraphSimilarityIndex(pl_complex),
        'rmsd_jaccard': lambda pl_complex: RMSDJaccard(pl_complex)
    }

    PENALTY = -100

    def __init__(self, pl_complex):
        self.pl_complex = pl_complex
        self.score = None

    @classmethod
    def get_reward_function(cls, pl_complex):
        config = Config.get_instance()
        function = cls.__reward_functions[config.reward_function]

        return function(pl_complex)

    def __call__(self):
        pass

    @property
    def is_legal(self):
        pass

    @property
    def perfect_fit(self):
        pass


class VinaReward(Reward):
    def __init__(self, pl_complex):
        Reward.__init__(self, pl_complex)
        self.vina_score = VinaScore(pl_complex.protein, pl_complex.ligand)

    def __call__(self):
        self.score = self.vina_score.total_energy()
        # return 1.05 ** (-2 * self.score)
        return -self.score

    def __str__(self):
        return f'VINA Score: {self.score}'

    @property
    def is_legal(self):
        return self.score is not None and self.score <= ComplexConstants.VINA_SCORE_THRESHOLD

    @property
    def perfect_fit(self):
        return self.score is not None and self.score <= ComplexConstants.VINA_GOOD_FIT


class RMSDReward(Reward):
    def __init__(self, pl_complex):
        Reward.__init__(self, pl_complex)
        self.rmsd = RMSD(pl_complex.ligand)

    def __call__(self):
        self.score = self.rmsd(self.pl_complex.original_ligand)
        rmsd_score = np.sinh(self.score + np.arcsinh(0))**-1

        return 200 if self.pl_complex.perfect_fit else rmsd_score

    def __str__(self):
        return f'RMSD: {np.round(self.score, 4)}'

    @property
    def is_legal(self):
        return self.score is not None and self.score <= ComplexConstants.RMSD_THRESHOLD

    @property
    def perfect_fit(self):
        return self.score is not None and self.score <= ComplexConstants.GOOD_FIT


class JaccardIndex(Reward):
    def __init__(self, pl_complex):
        super(JaccardIndex, self).__init__(pl_complex)
        self.actual_edge_set = None

    def __call__(self):
        if self.actual_edge_set is None:
            actual_edges = [
                edge for edge in self.pl_complex.actual_inter_molecular_interactions
                if edge.bond_type
            ]

            self.actual_edge_set = {
                tuple(edge.encoding()) + (edge.atom_a.idx, edge.atom_b.idx)
                for edge in actual_edges
            }

        current_edges = [
            edge for edge in self.pl_complex.all_inter_molecular_interactions
            if edge.bond_type
        ]

        current_edge_set = {
            tuple(edge.encoding()) + (edge.atom_a.idx, edge.atom_b.idx)
            for edge in current_edges
        }

        if len(self.actual_edge_set) == 0 and len(current_edge_set) == 0:
            self.score = 1
            return 0

        union_set = self.actual_edge_set.union(current_edge_set)
        intersection_set = self.actual_edge_set.intersection(current_edge_set)

        self.score = 1 - len(intersection_set)/len(union_set)

        return 10 * np.e**(-10 * self.score) if not self.perfect_fit else 200

    def __str__(self):
        return f'Jaccard DisIndex: {np.round(self.score, 4)}'

    @property
    def is_legal(self):
        return self.pl_complex.ligand_centroid_saperation < ComplexConstants.RMSD_THRESHOLD

    @property
    def perfect_fit(self):
        return self.score is not None and \
            (1 - self.score) >= ComplexConstants.JACCARD_MAX_THRESHOLD


class GraphSimilarityIndex(Reward):
    def __init__(self, pl_complex):
        super(GraphSimilarityIndex, self).__init__(pl_complex)

        self.actual_edge_dict = None

    def __call__(self):
        if self.actual_edge_dict is None:
            actual_edges = [
                edge for edge in self.pl_complex.actual_inter_molecular_interactions
                if edge.bond_type
            ]

            self.actual_edge_dict = {
                (edge.atom_a.idx, edge.atom_b.idx): edge
                for edge in actual_edges
            }

        current_edges = [
            edge for edge in self.pl_complex.all_inter_molecular_interactions
            if edge.bond_type
        ]

        current_edge_dict = {
            (edge.atom_a.idx, edge.atom_b.idx): edge
            for edge in current_edges
        }

        self.score = 1 - np.mean([
            edge.bond_similarity(current_edge_dict[key]) if key in current_edge_dict
            else 0
            for key, edge in self.actual_edge_dict.items()
        ])

        return 10 * np.e**(-10 * self.score) if not self.perfect_fit else 200

    def __str__(self):
        return f'GraphDisSimilarityScore: {np.round(self.score, 4)}'

    @property
    def is_legal(self):
        return self.pl_complex.ligand_centroid_saperation < ComplexConstants.RMSD_THRESHOLD

    @property
    def perfect_fit(self):
        return self.score is not None and \
            (1 - self.score) >= ComplexConstants.SIMILARITY_MAX_THRESHOLD


class RMSDJaccard(Reward):
    def __init__(self, pl_complex):
        Reward.__init__(self, pl_complex)
        self.rmsd = RMSDReward(pl_complex)
        self.jaccard_index = JaccardIndex(pl_complex)
        self.reward = 0

    def __call__(self):
        reward = self.jaccard_index() + 0.5 * self.rmsd()
        self.score = self.rmsd.score

        return 200 if self.jaccard_index.perfect_fit else reward

    def __str__(self):
        return f'RMSD: {np.round(self.rmsd.score, 4)},'\
               f' Jaccard DisIndex: {self.jaccard_index.score}'

    @property
    def is_legal(self):
        return self.rmsd.score is not None and self.score <= ComplexConstants.RMSD_THRESHOLD

    @property
    def perfect_fit(self):
        return self.rmsd.score is not None and self.score <= ComplexConstants.GOOD_FIT
