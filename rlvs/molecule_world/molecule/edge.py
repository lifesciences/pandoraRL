import numpy as np


class Edge:
    def __init__(self, node_a, node_b):
        self.node_a = node_a
        self.node_b = node_b
        self._distance = np.linalg.norm(node_a.coord - node_b.coord)

    @property
    def distance(self):
        self._distance = np.linalg.norm(self.node_a.coord - self.node_b.coord)
        return self._distance

    def update_bond_type(self, bond_type):
        if self.bond_type is None:
            self.bond_type = bond_type
        else:
            self.bond_type = self.bond_type | bond_type
