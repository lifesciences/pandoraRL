class AtomCollection:
    def __init__(self):
        self._atoms = []

    def __len__(self):
        return len(self._atoms)

    def __iter__(self):
        return self._atoms.__iter__()

    def where(self, condition):
        return [atom for atom in self._atoms if condition(atom)]

    def __getitem__(self, idx):
        return self._atoms[idx]

    def __contains__(self, atom):
        return atom in self._atoms

    def add(self, atom):
        self._atoms.append(atom)
