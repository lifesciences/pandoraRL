from openbabel import openbabel as ob
import networkx as nx
import numpy as np
from rlvs.constants import RESIDUES, Z_SCORES
from .residue_bond import ResidueBond
from .atom_collection import AtomCollection
from .types import ResidueBondType


class Residues:
    def __init__(self, obmol):
        self.residues = {
            ob_atom.GetResidue().GetIdx(): Residue(
                ob_atom.GetResidue().GetIdx(),
                ob_atom.GetResidue().GetName()
            )
            for ob_atom in ob.OBMolAtomIter(obmol)
        }

        self.residue_graph = nx.Graph()
        self.residue_bonds = []

    def __getitem__(self, key):
        return self.residues[key]

    def __iter__(self):
        return self.residues.values().__iter__()

    def add_bonds(self, bonds):
        self.residue_bonds = [
            ResidueBond(bond.atom_a.residue, bond.atom_b.residue, bond)
            for bond in bonds
            if bond.atom_a.residue.idx != bond.atom_b.residue.idx
        ]

    def crop(self, bonds, atoms):
        residues = {}
        for atom in atoms:
            residue = residues.get(
                atom.residue.idx,
                Residue.clone(atom.residue)
            )

            atom.residue = residue
            residue.add_atom(atom)
            residues[residue.idx] = residue

        self.residues = residues
        self.add_bonds(bonds)
        self.add_intra_molecular_bonds()
        self.update_residue_graph()

    def add_intra_molecular_bonds(self):
        residues = [residue for residue in self.residues.values()]

        adg_mat = np.array(np.meshgrid(residues, residues)).T.reshape(-1, 2)
        intra_residue_bonds = [
            ResidueBond(
                residue_a, residue_b, None, non_covalent=True
            ) for residue_a, residue_b in adg_mat
            if residue_a != residue_b
        ]
        for bond in intra_residue_bonds:
            bond.add_hydrogen_bond()

        self.residue_bonds += intra_residue_bonds

    def update_residue_graph(self):
        self.residue_graph = nx.Graph()

        self.residue_graph.add_nodes_from([
            (residue.idx, {
                'name': residue.name,
            }) for residue in self.residues.values()
        ])

        self.residue_graph.add_edges_from([
            (bond.residue_a.idx, bond.residue_b.idx, {'bond': bond})
            for bond in self.residue_bonds
            if bond.bond_type != ResidueBondType.UNNAMED
        ])


class Residue(AtomCollection):
    def __init__(self, idx, name, c_alpha=None):
        super(Residue, self).__init__()
        self.idx = idx
        self.name = name.upper()
        self.c_alpha = c_alpha

        self.residue_bonds = []

    @staticmethod
    def clone(residue):
        return Residue(residue.idx, residue.name, residue.c_alpha)

    @property
    def coord(self):
        return self.c_alpha.coord

    def remove_atom(self, atom):
        self._atoms.remove(atom)

    def add_atom(self, atom):
        if atom.is_c_alpha:
            self.c_alpha = atom

        self.add(atom)

    def add_bond(self, residue_bond):
        self.residue_bonds.append(residue_bond)

    def get_residue_group_encoding(residue, residue_grouping):
        unique_groups = np.unique(list(residue_grouping.values()))
        max_group_num = max(unique_groups)
        encoding_vector = np.zeros(len(unique_groups))

        if residue is None:
            return encoding_vector

        encoding_vector[
            residue_grouping.get(residue.name, max_group_num)
        ] = 1

        return encoding_vector

    def encoding(residue):
        encoding = np.zeros(len(RESIDUES), dtype=int)
        if residue is None:
            return encoding

        encoding[RESIDUES.get(residue.name, 20)] = 1

        return encoding

    def z_scores(residue):
        if residue is None:
            return [0, 0, 0, 0, 0]
        return Z_SCORES.get(residue.name, [0, 0, 0, 0, 0])
