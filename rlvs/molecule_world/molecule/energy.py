class Energy:
    def __init__(
            self, gauss1=0, gauss2=0, repulsion=0,
            hydrophobic=0, hydrogen_bond=0
    ):
        self.gauss1 = gauss1
        self.gauss2 = gauss2
        self.repulsion = repulsion
        self.hydrophobic = hydrophobic
        self.hydrogen_bond = hydrogen_bond

    def __setitem__(self, key, value):
        setattr(self, key, value)

    @property
    def binding_affinity(self):
        W1 = -0.0356
        W2 = -0.00516
        W3 = 0.840
        W4 = -0.0351
        W5 = -0.587
        bind_aff = W1 * self.gauss1 + W2 * self.gauss2 +\
            W3 * self.repulsion + W4 * self.hydrophobic +\
            W5 * self.hydrogen_bond

        return bind_aff
