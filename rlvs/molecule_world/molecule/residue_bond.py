import numpy as np
from .edge import Edge
from .types import ResidueBondType, BondType
from .inter_molecular_bond import HydrogenBond


class ResidueBond(Edge):
    def __init__(self, residue_a, residue_b, atomic_bond, non_covalent=False):
        super(ResidueBond, self).__init__(residue_a, residue_b)
        self.residue_a = residue_a
        self.residue_b = residue_b
        self.atomic_bond = atomic_bond
        residue_a.add_bond(self)
        residue_b.add_bond(self)
        self.bond_type = ResidueBondType.UNNAMED if non_covalent else ResidueBondType.COVALENT

    def add_hydrogen_bond(self):
        residue_a_acceptors = self.residue_a.where(lambda atom: atom.acceptor)
        residue_b_acceptors = self.residue_b.where(lambda atom: atom.acceptor)
        residue_a_donors = self.residue_a.where(lambda atom: atom.has_hydrogen)
        residue_b_donors = self.residue_b.where(lambda atom: atom.has_hydrogen)
        for comb in [
                (residue_a_donors, residue_b_acceptors),
                (residue_b_donors, residue_a_acceptors)
        ]:
            agt_mat = np.array(np.meshgrid(*comb)).T.reshape(-1, 2)
            for hydrogen_bond_couple in agt_mat:
                bond = HydrogenBond(None, *hydrogen_bond_couple)
                if BondType.is_hydrogen_bond(*hydrogen_bond_couple) and bond.is_valid:
                    self.atomic_bond = bond
                    self.update_bond_type(ResidueBondType.HYDROGEN)
