import numpy as np
import torch
from torch_geometric import utils
import networkx as nx

from rlvs.constants import ComplexConstants
from rlvs.molecule_world.scoring import VinaScore, RMSD
from .types import BondType
from .inter_molecular_bond import InterMolecularBond, BondEncoder
from .bonds import Bonds

import logging


class Complex:
    def __init__(self, protein, ligand, original_ligand):
        self.protein = protein
        self.ligand = ligand
        self.original_ligand = original_ligand
        self.vina = VinaScore(protein, ligand, pl_complex=self)
        self.all_inter_molecular_interactions = self.all_inter_molecular_bonds()
        self.actual_inter_molecular_interactions = self.all_inter_molecular_bonds(original=True)
        self.complex_graph = self.create_complex_graph()
        self.update_edges()

        logging.debug(
            f"""complex Stats: Ligand Shape: {self.ligand.data.x.shape},
            Protein Shape: {self.protein.data.x.shape},
            Initial Vina Score: {self.vina.total_energy()}
            """
        )

    @property
    def n_node_features(self):
        return self.protein.get_atom_features().shape[1]

    @property
    def n_edge_features(self):
        return len(self.all_inter_molecular_interactions[0].feature)

    @property
    def valid_edges(self):
        return [
            (atom_a, atom_b, {'features': data['bond'].feature})
            for atom_a, atom_b, data in self.complex_graph.edges(data=True)
            if (
                    data['bond'].bond_type == BondType.COVALENT
            ) or (
                data['bond'].distance <= ComplexConstants.DISTANCE_THRESHOLD and
                data['bond'].bond_type is not None and
                data['bond'].bond_type != 0
            )
        ]

    def get_picklable_graph(self):
        graph = nx.Graph()
        graph.add_nodes_from(
            (idx, {
                'atom_idx': d['atom'].atom_idx,
                'name': d['atom'].name,
                'residue': d['atom'].residue.name if d[
                    'atom'
                ].residue else 'NA',
                'mol_type': 'protein' if d[
                    'molecule_type'
                ] == 0 else 'ligand'
            })
            for idx, d in list(self.complex_graph.nodes(data=True))
        )

        graph.add_edges_from([
            (a, b, {
                'bond_type': data['bond'].bond_type,
                'encoding': data['bond'].encoding()
            })
            for a, b, data in self.complex_graph.edges(data=True)
        ])

        return graph

    def create_complex_graph(self):
        n_p_atoms = len(self.protein.atoms)
        complex_graph = nx.compose(
            self.protein.molecule_graph,
            nx.relabel_nodes(
                self.ligand.molecule_graph, {
                    x: x + len(self.protein.atoms)
                    for x in self.ligand.molecule_graph.nodes
                }
            )
        )

        complex_graph.add_edges_from([
            (bond.atom_a.idx, bond.atom_b.idx + n_p_atoms, {'bond': bond})
            for bond in self.all_inter_molecular_interactions
        ])

        return complex_graph

    def crop(self, x, y, z):
        self.protein.crop(self.ligand.get_centroid(), x, y, z)
        self.all_inter_molecular_interactions = self.all_inter_molecular_bonds()
        self.actual_inter_molecular_interactions = self.all_inter_molecular_bonds(original=True)
        self.complex_graph = self.create_complex_graph()
        self.update_edges(update_original=True)

    def update_edges(self, update_original=False):
        for edge in self.all_inter_molecular_interactions:
            edge.bond_type = 0
            BondEncoder.generate_encoded_bond_types(edge, mutate=True)
            edge.reset_interaction_strengths()
            edge.update_interaction_strengths()

        self.vina.update_bond_energy()

        if update_original:
            for edge in self.actual_inter_molecular_interactions:
                edge.bond_type = 0
                BondEncoder.generate_encoded_bond_types(edge, mutate=True)
                edge.reset_interaction_strengths()
                edge.update_interaction_strengths()

    def vina_score(self):
        return self.vina.total_energy()

    def update_pose(self, x=0, y=0, z=0, roll=0, pitch=0, yaw=0):
        self.ligand.update_pose(x, y, z, roll, pitch, yaw)
        self.update_edges()

    def all_inter_molecular_bonds(self, original=False):
        ligand = self.original_ligand if original else self.ligand
        n_p_atoms = len(self.protein.atoms)
        p_atoms = [atom.idx for atom in self.protein.atoms.where(lambda x: x.is_heavy_atom)]
        l_atoms = [atom.idx for atom in ligand.atoms.where(lambda x: x.is_heavy_atom)]

        adg_mat = np.array(np.meshgrid(l_atoms, p_atoms)).T.reshape(-1, 2)

        return Bonds([
            InterMolecularBond(
                self.protein.atoms[p_idx],
                ligand.atoms[l_idx],
                None,
                update_edge=False,
                ligand_offset=n_p_atoms,
                bond_type=0
            ) for l_idx, p_idx in adg_mat
        ])

    def randomize_ligand(self, action_shape, test=False):
        self.ligand.randomize(ComplexConstants.BOUNDS, action_shape, test=test)
        self.update_edges()

    def reset_ligand(self):
        self.ligand.set_coords(self.original_ligand.get_coords())

    @property
    def rmsd(self):
        return self.ligand.rmsd(self.original_ligand)

    @property
    def ligand_centroid_saperation(self):
        original_ligand_centroid = RMSD.centroid(self.original_ligand.get_coords())
        current_ligand_centroid = RMSD.centroid(self.ligand.get_coords())

        return np.linalg.norm(original_ligand_centroid - current_ligand_centroid)

    @property
    def perfect_fit(self):
        rmsd = self.ligand.rmsd(self.original_ligand)
        return rmsd < ComplexConstants.GOOD_FIT

    @property
    def data(self):
        data_graph = nx.Graph()
        data_graph.add_nodes_from([
            (n, {'features': data['features']()})
            for n, data in self.complex_graph.nodes(data=True)
        ])

        batch = torch.tensor([0] * len(self.complex_graph.nodes))
        data_graph.add_edges_from(self.valid_edges)

        data = utils.from_networkx(
            data_graph,
            group_node_attrs=['features'],
            group_edge_attrs=['features']
        )
        data.batch = batch

        data.x = data.x.type(torch.FloatTensor)
        data.edge_attr = data.edge_attr.type(torch.FloatTensor)

        return data

    def save(self, path, filetype=None):
        ligand_filetype = filetype if filetype is not None else self.ligand.filetype
        ligand_name = self.ligand.path.split('/')[-1].split('.')[0]
        self.ligand.save(f'{path}_{ligand_name}.{ligand_filetype}', ligand_filetype)
