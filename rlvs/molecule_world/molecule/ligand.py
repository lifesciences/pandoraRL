from .atom import Atoms
from .molecule import Molecule
from .types import MoleculeType

import numpy as np
import pandas as pd

from ..helper_functions import read_to_OB, randomizer
import logging


class Ligand(Molecule):
    molecule_type = MoleculeType.LIGAND

    def __init__(self, path=None, name=None, filetype=None):
        super(Ligand, self).__init__(path, filetype=filetype)
        obmol = read_to_OB(filename=path, filetype=filetype)

        fg = self.__get_functional_groups(path)
        self.atoms = Atoms(self.molecule_type, obmol, fg=fg)

        self.atom_features = self.atoms.features

    def __get_functional_groups(self, path):
        file_path_without_ext = '.'.join(path.split('.')[:-1])

        try:
            fg_path = f'{file_path_without_ext}_fg.csv'
            fg_df = pd.read_csv(fg_path)
            fg_df = fg_df.sort_values(by=['Index'])
            fg = fg_df.loc[:, ['Index', 'FG']].values
            return fg
        except FileNotFoundError:
            return None

    def randomize(self, box_size, action_shape, test=False):
        random_pose = randomizer(action_shape, test)
        print("Randomized", random_pose)
        logging.info(f'Randomized Pose: {random_pose}')
        self.update_pose(*random_pose)
