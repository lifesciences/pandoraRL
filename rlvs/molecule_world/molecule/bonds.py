class Bonds:
    def __init__(self, bonds):
        self.bonds_dict = {
            (bond.atom_a.idx, bond.atom_b.idx): bond
            for bond in bonds
        }
        self.bonds = bonds

    def __getitem__(self, key):
        if type(key) is int:
            return self.bonds[key]
        else:
            return self.bonds_dict[key]

    def __iter__(self):
        return self.bonds.__iter__()

    def __len__(self):
        return len(self.bonds)

    def where(self, predicate):
        return Bonds([bond for bond in self.bonds if predicate(bond)])

    def update_energy(self, key, energy):
        if key in self.bonds_dict:
            bond = self.bonds_dict[key]
            bond.energy = energy

        if key in self.bonds:
            bond = self.bonds[key]
            bond.energy = energy
