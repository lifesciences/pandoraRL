import numpy as np
import os

class DataCache:
    def __init__(self, output_path):
        self.train_datastore_path = output_path + '_datastore_{train_episode}{episode}_{iteration}.npy'
        self.test_datastore_path = output_path + '_test_datastore_{train_episode}_{episode}_{iteration}.npy'

    def add_data(self, graph, data, rmsd, episode, iteration, test=False, train_episode=''):
        datastore = [graph, data, rmsd]
        datapath = self.train_datastore_path
        if test:
            datapath = self.test_datastore_path
        with open(datapath.format(
                episode=episode,
                iteration=iteration,
                train_episode=train_episode
        ), 'wb') as datafile:
            np.save(datafile, datastore)

    def get_data(self, episode, iteration, test=False, train_episode=''):
        datapath = self.train_datastore_path
        if test:
            datapath = self.test_datastore_path

        with open(datapath.format(
                episode=episode,
                iteration=iteration,
                train_episode=train_episode
        ), 'rb') as datafile:
            datastore = np.load(datafile, allow_pickle=True)

        return datastore

    def clear_data(self, episode, iteration, test=False, train_episode=''):
        datapath = self.train_datastore_path
        if test:
            datapath = self.test_datastore_path

        try:
            file_to_remove = datapath.format(
                episode=episode,
                iteration=iteration,
                train_episode=train_episode
            )

            print(f'Deleting {file_to_remove}')
            os.remove(file_to_remove)
        except Exception as e:
            print(e)

    def clear_multiple(self, episode, iterations, test=False, train_episode=''):
        for iteration in range(iterations):
            self.clear_data(episode, iteration, test, train_episode)
