import os
from os import path
import numpy as np
from rlvs.config import Config
from sklearn.preprocessing import MinMaxScaler
from ..molecule.protein import Protein
from ..molecule.ligand import Ligand
from ..molecule.complex import Complex

ROOT_PATH = f'{path.dirname(path.abspath(__file__))}/../../../data'


class Data:
    DATA_PATH = None
    LIGAND_FILE_TYPE = 'sdf'
    PROTEIN_FILE_TYPE = 'pdb'
    complexes_path = None

    @classmethod
    def protein_path(cls, protein):
        pass

    @classmethod
    def ligand_path(cls, ligand):
        pass

    @classmethod
    def complex_paths(cls):
        if cls.complexes_path is not None:
            return cls.complexes_path

        complexes_path = os.listdir(cls.DATA_PATH)
        if '.DS_Store' in complexes_path:
            complexes_path.remove('.DS_Store')
        if 'affinities.csv' in complexes_path:
            complexes_path.remove('affinities.csv')

        return complexes_path

    def complexes(self):
        return [
            Complex(
                Protein(
                    path=self.protein_path(complex),
                    filetype=self.PROTEIN_FILE_TYPE
                ), Ligand(
                    path=self.ligand_path(complex),
                    filetype=self.LIGAND_FILE_TYPE
                ), Ligand(
                    path=self.ligand_path(complex), # <path_tofile>/ligand.sdf
                    filetype=self.LIGAND_FILE_TYPE
                )
            ) for complex in self.complexes_path
        ]

    def __init__(self):
        self.complexes_path = self.complex_paths()
        self._complexes = self.complexes()

    def get_molecules(self, complex, crop=True):
        if crop:
            complex.crop(10, 10, 10)

        return complex

    @property
    def random(self):
        return self._complexes[np.random.randint(len(self._complexes))]


class PafnucyData(Data):
    DATA_PATH = f'{ROOT_PATH}/pafnucy_data/complexes'

    @classmethod
    def ligand_path(cls, ligand, filetype=None):
        filetype = cls.LIGAND_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{ligand}/{ligand}_ligand.{filetype}'

    @classmethod
    def protein_path(cls, protein, filetype=None):
        filetype = cls.PROTEIN_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{protein}/{protein}_pocket.{filetype}'

    def __init__(self):
        super(PafnucyData, self).__init__()


class PafnucyTest(PafnucyData):
    DATA_PATH = f'{ROOT_PATH}/pafnucy_test/complexes'

    def __init__(self):
        super(PafnucyTest, self).__init__()


class SARSVarients(Data):
    DATA_PATH = f'{ROOT_PATH}/SARS_variants/'

    @classmethod
    def ligand_path(cls, ligand, filetype=None):
        filetype = cls.LIGAND_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{ligand}/{ligand}_ligand.{filetype}'

    @classmethod
    def protein_path(cls, protein, filetype=None):
        filetype = cls.PROTEIN_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{protein}/{protein}_protein.{filetype}'

    def __init__(self):
        super(SARSVarients, self).__init__()


class SARSTest(SARSVarients):
    DATA_PATH = f'{ROOT_PATH}/SARS_test/'

    def __init__(self):
        super(SARSTest, self).__init__()


class SARSCov2(SARSVarients):
    DATA_PATH = f'{ROOT_PATH}/SARS_COV2'

    def __init__(self):
        super(SARSCov2, self).__init__()


class PDBQTData_2(Data):
    DATA_PATH = f'{ROOT_PATH}/pdbqt_data_2'

    @classmethod
    def ligand_path(cls, ligand, filetype=None):
        filetype = cls.LIGAND_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{ligand}/{ligand}_ligand.{filetype}'

    @classmethod
    def protein_path(cls, protein, filetype=None):
        filetype = cls.PROTEIN_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{protein}/{protein}_protein.{filetype}'

    def __init__(self):
        super(PDBQTData_2, self).__init__()


class DudeProteaseData(Data):
    DATA_PATH = f'{ROOT_PATH}/dude_protease'

    @classmethod
    def ligand_path(cls, ligand, filetype=None):
        filetype = cls.LIGAND_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{ligand}/crystal_ligand.{filetype}'

    @classmethod
    def protein_path(cls, complex, filetype=None):
        filetype = cls.PROTEIN_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{complex}/receptor.{filetype}'

    def __init__(self):
        super(DudeProteaseData, self).__init__()


class MERSVariants(Data):
    DATA_PATH = f'{ROOT_PATH}/MERS_variants'

    @classmethod
    def ligand_path(cls, ligand, filetype=None):
        filetype = cls.LIGAND_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{ligand}/{ligand}-ligand.{filetype}'

    @classmethod
    def protein_path(cls, protein, filetype=None):
        filetype = cls.PROTEIN_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{protein}/{protein}-protein.{filetype}'

    def __init__(self):
        super(MERSVariants, self).__init__()


class MERSTest(MERSVariants):
    DATA_PATH = f'{ROOT_PATH}/MERS_test'

    def __init__(self):
        super(MERSTest, self).__init__()


class BATVariants(Data):
    DATA_PATH = f'{ROOT_PATH}/BAT_variants'

    @classmethod
    def ligand_path(cls, ligand, filetype=None):
        filetype = cls.LIGAND_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{ligand}/{ligand}-ligand.{filetype}'

    @classmethod
    def protein_path(cls, protein, filetype=None):
        filetype = cls.PROTEIN_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{protein}/{protein}-protein.{filetype}'

    def __init__(self):
        super(BATVariants, self).__init__()


class BATTest(BATVariants):
    DATA_PATH = f'{ROOT_PATH}/BAT_test'

    def __init__(self):
        super(BATTest, self).__init__()


class HIVVariants(Data):
    DATA_PATH = f'{ROOT_PATH}/HIV_variants'

    @classmethod
    def ligand_path(cls, ligand, filetype=None):
        filetype = cls.LIGAND_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{ligand}/{ligand}-ligand.{filetype}'

    @classmethod
    def protein_path(cls, protein, filetype=None):
        filetype = cls.PROTEIN_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{protein}/{protein}-protein.{filetype}'

    def __init__(self):
        super(HIVVariants, self).__init__()


class HIVTest(HIVVariants):
    DATA_PATH = f'{ROOT_PATH}/HIV_test'

    def __init__(self):
        super(HIVTest, self).__init__()


class PLProVariants(Data):
    DATA_PATH = f'{ROOT_PATH}/PL-Pro_variants'

    @classmethod
    def ligand_path(cls, ligand, filetype=None):
        filetype = cls.LIGAND_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{ligand}/{ligand}-ligand.{filetype}'

    @classmethod
    def protein_path(cls, protein, filetype=None):
        filetype = cls.PROTEIN_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{protein}/{protein}-protein.{filetype}'

    def __init__(self):
        super(PLProVariants, self).__init__()


class PLProTest(PLProVariants):
    DATA_PATH = f'{ROOT_PATH}/PL-Pro_test'

    def __init__(self):
        super(PLProTest, self).__init__()

class SingleComplexTrain(Data):
    DATA_PATH = f'{ROOT_PATH}/Single_complex_train/'

    @classmethod
    def ligand_path(cls, ligand, filetype=None):
        filetype = cls.LIGAND_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{ligand}/{ligand}_ligand.{filetype}'

    @classmethod
    def protein_path(cls, protein, filetype=None):
        filetype = cls.PROTEIN_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{protein}/{protein}_protein.{filetype}'

    def __init__(self):
        super(SingleComplexTrain, self).__init__()


class SingleComplexTest(SingleComplexTrain):
    DATA_PATH = f'{ROOT_PATH}/Single_complex_test/'

    def __init__(self):
        super(SingleComplexTest, self).__init__()

class SARSShuffleTrain(Data):
    DATA_PATH = f'{ROOT_PATH}/SARS_shuffle_train/'

    @classmethod
    def ligand_path(cls, ligand, filetype=None):
        filetype = cls.LIGAND_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{ligand}/{ligand}_ligand.{filetype}'

    @classmethod
    def protein_path(cls, protein, filetype=None):
        filetype = cls.PROTEIN_FILE_TYPE if filetype is None else filetype
        return f'{cls.DATA_PATH}/{protein}/{protein}_protein.{filetype}'

    def __init__(self):
        super(SARSShuffleTrain, self).__init__()


class SARSShuffleTest(SARSShuffleTrain):
    DATA_PATH = f'{ROOT_PATH}/SARS_shuffle_test/'

    def __init__(self):
        super(SARSShuffleTest, self).__init__()

class DataStore:
    REGISTRY = {
      'PafnucyData': PafnucyData,
      'PafnucyTest': PafnucyTest,
      'SARSVarients': SARSVarients,
      'SARSTest': SARSTest,
      'SARSCov2': SARSCov2,
      'PDBQTData_2': PDBQTData_2,
      'DudeProteaseData': DudeProteaseData,
      'MERSVariants': MERSVariants,
      'MERSTest': MERSTest,
      'PLProVariants': PLProVariants,
      'PLProTest': PLProTest,
      'BATVariants': BATVariants,
      'BATTest': BATTest,
      'HIVVariants': HIVVariants,
      'HIVTest': HIVTest,
      'SingleComplexTrain': SingleComplexTrain,
      'SingleComplexTest': SingleComplexTest,
      'SARSShuffleTrain': SARSShuffleTrain,
      'SARSShuffleTest': SARSShuffleTest
    }
    DATA_STORES = []
    TEST_DATA_STORES = []
    DATA = []
    TEST_DATA = []

    @classmethod
    def init(cls, crop=True):
        config = Config.get_instance()
        cls.DATA_STORES = [cls.REGISTRY[data_store]() for data_store in config.train_dataset]
        cls.TEST_DATA_STORES = [cls.REGISTRY[data_store]() for data_store in config.test_dataset]

        cls.load(crop)
        # cls.scaler = cls.normalize()

    @classmethod
    def load(cls, crop=True):
        cls.DATA = [
            store.get_molecules(complex, crop)
            for store in cls.DATA_STORES
            for complex in store._complexes
        ]

        cls.TEST_DATA = [
            store.get_molecules(complex, crop)
            for store in cls.TEST_DATA_STORES
            for complex in store._complexes
        ]

    @classmethod
    def next(cls, crop=True, test=False):
        data = cls.DATA
        if test:
            data = cls.TEST_DATA

        return data[np.random.randint(0, len(data))]

    @classmethod
    def normalize(cls):
        X = []
        for complex in cls.DATA:
            protein, ligand = complex.protein, complex.ligand
            X.extend(list(protein.get_atom_features()))
            X.extend(list(ligand.get_atom_features()))
        X = np.asarray(X)
        scaler = MinMaxScaler()
        scaler.fit(X)
        return scaler
