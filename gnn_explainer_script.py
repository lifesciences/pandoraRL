from rlvs.profiler.gnn_explainer.gnn_explainer_process import GNNExplainerServer
from rlvs.config import Config
import os

config_path = os.getenv('CONFIG', './config.json')
Config.init(config_path)

if __name__ == '__main__':
    gnn_explainer_server = GNNExplainerServer()
    gnn_explainer_server.run_explainer()
