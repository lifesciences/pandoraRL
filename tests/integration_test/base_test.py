from rlvs.molecule_world.env import GraphEnv
from rlvs.agents.dqn_agent import DQNAgentGNN
from rlvs.agents.ddpg_agent import DDPGAgentGNN
from rlvs.config import Config
import numpy as np

import logging
from os import path
import os

import pytest


class TrainingAgents:
    @staticmethod
    def get_agent(algorithm: str):
        agents = {
            'dqn': DQNAgentGNN,
            'ddpg': DDPGAgentGNN
        }

        return agents.get(algorithm)


class BaseTest:
    ROOT_PATH = f'{path.dirname(path.abspath(__file__))}'

    @pytest.fixture(autouse=True)
    def setup_and_teardown(self):
        self.model_output_path = f'{self.ROOT_PATH}/model_output/'
        self.init_config()
        path_prefix = f"{self.model_output_path}"
        log_filename = f"{path_prefix}/training_log.log"

        logging.basicConfig(
            filename=log_filename,
            filemode='w',
            format='%(message)s',
            datefmt='%I:%M:%S %p',
            level=logging.DEBUG
        )

        self.agent = self.get_agent(path_prefix)

        yield

        self.cleanup()

    def init_config(self, config_path=None):
        config_path = f'{self.model_output_path}/config.json' \
            if config_path is None else config_path

        Config.init(config_path)

    def get_agent(self, path_prefix=None):
        path_prefix = self.model_output_path\
            if path_prefix is None else path_prefix

        config = Config.get_instance()
        env = GraphEnv(single_step=np.array(config.single_step))
        training_agent = TrainingAgents.get_agent(config.training_algorithm)

        return training_agent(
            env,
            weights_path=path_prefix+"weights_intermediate",
            complex_path=path_prefix+"ligand_intermediate"
        )

    def artifacts(self):
        return os.listdir(self.model_output_path)

    def cleanup(self):
        artifacts = self.artifacts()
        for artefact in artifacts:
            if artefact != 'config.json':
                os.remove(f'{self.model_output_path}/{artefact}')
