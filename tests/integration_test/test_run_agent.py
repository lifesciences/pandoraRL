from .base_test import BaseTest, Config


class TestRunAgent(BaseTest):

    def test_run_agent(self):
        config = Config.get_instance()
        config.episode_length = 50
        self.agent.play(1)
        artefacts = self.artifacts()

        assert len([name for name in artefacts if name.endswith('actor')]) == 1
        assert len([name for name in artefacts if name.endswith('.png')]) == 2
        assert len([name for name in artefacts if name.endswith('.pdb')]) == 1
        assert len([name for name in artefacts if name.endswith('.npy')]) == 1
