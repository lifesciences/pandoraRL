from openbabel import pybel
from openbabel import openbabel as ob
from os import path
from rlvs.molecule_world.helper_functions import read_to_OB
from rlvs.molecule_world.datastore import datasets

ROOT_PATH = f'{path.dirname(path.abspath(__file__))}/data'


def convert_obmol(obmol, dest_path, in_filetype, out_filetype='pdb', prepare=False):
    inter_filetype = "mol2" if prepare and in_filetype != "mol2" else out_filetype

    obConversion = ob.OBConversion()
    obConversion.SetInAndOutFormats(in_filetype, inter_filetype)
    pdb_content = obConversion.WriteString(obmol)

    if prepare:
        if in_filetype != "mol2":
            obConversion = ob.OBConversion()
            obConversion.SetInAndOutFormats(inter_filetype, out_filetype)
            obmol_c = ob.OBMol()
            obConversion.ReadString(obmol_c, pdb_content)
        else:
            obmol_c = obmol

        mol_py = pybel.Molecule(obmol_c)
        mol_py.addh()
        mol_py.calccharges(model="gasteiger")
        obmol_c = mol_py.OBMol
        pdb_content = obConversion.WriteString(obmol_c)

    with open(dest_path, 'w') as pdb_file:
        print(f"writing {out_filetype} {'prepared' if prepare else ''}")
        pdb_file.write(pdb_content)


def migrate_ligand(dataset, prepare=False):
    out_filetype = "sdf"
    files = [
            {
                "obmol": read_to_OB(
                    dataset.ligand_path(complex_),
                    dataset.LIGAND_FILE_TYPE
                ),
                "dest_path": dataset.ligand_path(complex_, filetype=out_filetype),
                "in_filetype": dataset.LIGAND_FILE_TYPE,
                "out_filetype": out_filetype
             } for complex_ in dataset.complex_paths()
        ]

    for args in files:
        convert_obmol(**args, prepare=prepare)


if __name__ == '__main__':
    migrate_ligand(datasets.PLProTest, prepare=True)
